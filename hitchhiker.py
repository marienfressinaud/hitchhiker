import sys
import feedparser
import jinja2
import urllib


def get_domain(url):
    return urllib.parse.urlparse(url).netloc


if __name__ == "__main__":
    entries = []
    for url in sys.argv[1:]:
        entries.extend(feedparser.parse(url).entries)

    seen = set()
    entries = [
        seen.add(entry.link) or entry for entry in entries if entry.link not in seen
    ]
    entries.sort(key=lambda entry: entry.published, reverse=True)
    entries = entries[:20]

    environment = jinja2.Environment(
        loader=jinja2.FileSystemLoader("templates"),
        autoescape=jinja2.select_autoescape(),
    )
    environment.filters["domain"] = get_domain

    template_index = environment.get_template("index.html.j2")
    print(template_index.render(entries=entries))
