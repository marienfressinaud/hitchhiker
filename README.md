# Hitchhiker

Hitchhiker est un prototype de générateur de planets statiques.

Un planet est un site Web qui agrège les publications de plusieurs sites, généralement sur un sujet précis.
Pour cela, il utilise les flux Web (<abbr>RSS</abbr>, Atom) mis à disposition par les sites concernés.

Hitchhiker se veut le plus minimal possible (30 lignes de code) afin de servir comme démonstration.

## Utilisation

Hitchhiker s’utilise en ligne de commande.
Il prend en argument la liste des flux Web à agréger.
Il affiche sur la sortie standard le code du planet : un simple fichier `index.html`.

```console
$ python3 hitchhiker.py \
    https://marienfressinaud.fr/feeds/all.atom.xml \
    https://flus.fr/carnet/feeds/all.atom.xml \
    https://app.flus.fr/p/1670839367044869607/feed.atom.xml?direct=true \
    > index.html
```

## Technique

Hitchhiker utilise deux dépendances :

- [feedparser](https://github.com/kurtmckee/feedparser) pour récupérer et parser les flux Web
- [Jinja2](https://jinja.palletsprojects.com/) pour générer le <abbr>HTML</abbr> à partir d’un template

Le template est disponible dans [le fichier `templates/index.html.j2`](/templates/index.html.j2).

## Évolutions possibles

Beaucoup d’améliorations sont possibles :

- personnaliser le template
- générer un flux des articles agrégés
- afficher le contenu des articles
- améliorer le support des flux (seuls les flux Atom bien formés sont correctement supportés)
- proposer un fichier de configuration pour personnaliser le titre du site, ou la manière de générer les entrées en fonction des flux
- proposer de la documentation pour publier les planets sur GitHub et Gitlab Pages
- mettre en cache les flux

## Licence

Hitchhiker est disponible sous [licence <abbr>AGPL</abbr> v3.0 ou plus](/LICENSE.txt).
